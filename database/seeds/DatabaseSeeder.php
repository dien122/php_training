<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();

        $this->call(UserTableSeeder::class);

        //Model::reguard();
    }
}

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the departments seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert(
            [
                array('department_name'=>'Phòng nhân sự', 'office_phone'=>'0123456789'),
                array('department_name'=>'Phòng HCTH', 'office_phone'=>'01234567895'),
            ]
        );
    }
}

class JobTableSeeder extends Seeder
{
    /**
     * Run the jobs seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert(
            [
                array('job_title'=>'Lập trình web'),
                array('job_title'=>'Lập trình android'),
            ]
        );
    }
}

class UserTableSeeder extends Seeder
{
    /**
     * Run the users seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
            array('name'=>'admin', 'email'=>'admin@gmail.com', 'password'=>Hash::make('admin'), 'level'=>1, 'birth'=>'01/01/1991', 'photo'=>'', 'phone'=>'0123456', 'sex'=>'1', 'description'=>'Hello I’m Jenifer Smith, a leading expert in interactive and creative design specializing in the mobile medium. My graduation from Massey University with a Bachelor of Design majoring in visual communication.', 'department_id'=>4, 'job_id'=>2)
            ]
        );
    }
}