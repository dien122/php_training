<aside>
	<div id="sidebar"  class="nav-collapse ">
	  <!-- sidebar menu start-->
	  <ul class="sidebar-menu">                
	      <li>
	          <a class="" href="{!! route('admin.department.getList') !!}">
	              <i class="icon_group"></i>
	              <span>Department</span>
	          </a>
	      </li>
	      <li>                     
	          <a class="" href="{!! route('admin.job.getList') !!}">
	              <i class="icon_briefcase_alt"></i>
	              <span>Job</span>
	          </a>                  
	      </li>
	      <li>
	          <a class="" href="{!! route('admin.user.getList') !!}">
	              <i class="icon_profile"></i>
	              <span>User</span>
	          </a>
	      </li>
	  </ul>
	  <!-- sidebar menu end-->
	</div>
</aside>