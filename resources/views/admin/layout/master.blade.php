<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Blank | Creative - Bootstrap 3 Responsive Admin Template</title>

    <!-- Bootstrap CSS -->    
    <link href="{{ url('public/admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{{ url('public/admin/css/bootstrap-theme.css') }}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{{ url('public/admin/css/elegant-icons-style.css') }}" rel="stylesheet" />
    <link href="{{ url('public/admin/css/font-awesome.min.css') }}" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="{{ url('public/admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('public/admin/css/style-responsive.css') }}" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
      <!--header start-->
      @include('admin.layout.include.header')
      <!--header end-->
      <!--sidebar start-->
      @include('admin.layout.include.sidebar')
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
      		  <div class="row">
      				<div class="col-lg-12">
      					<h3 class="page-header"><i class="fa fa fa-bars"></i> Pages</h3>
      					<ol class="breadcrumb">
      						<li><i class="fa fa-home"></i><a href="#">Home</a></li>
      						<li><i class="fa fa-bars"></i>Pages</li>
      						<li><i class="fa fa-square-o"></i>Pages</li>
      					</ol>
      				</div>
              <div class="col-lg-12">
                @if(Session::has('flash-message'))
                    <div id="notification" class="alert alert-{!! Session::get('flash-level') !!} hide">
                        {!! Session::get('flash-message') !!}
                    </div>
                @endif
              </div>
      			</div>
            <!-- page start-->
            @yield('content');
            <!-- page end-->
          </section>
      </section>
      <!--main content end-->
  </section>
  <!-- container section end -->
    <!-- javascripts -->
    <script src="{{ url('public/admin/js/jquery.js') }}"></script>
    <script src="{{ url('public/admin/js/jquery-1.8.3.min.js') }}"></script>
    <script src="{{ url('public/admin/js/bootstrap.min.js') }}"></script>
    <!-- nice scroll -->
    <script src="{{ url('public/admin/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ url('public/admin/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
    <!--custome script for all page-->
    <script src="{{ url('public/admin/js/scripts.js') }}"></script>
    <script src="{{ url('public/admin/js/myscript.js') }}" type="text/javascript"></script>
    <!-- ck editor -->
    <script type="text/javascript" src="{!! url('public/admin/assets/ckeditor/ckeditor.js') !!}"></script>
    <script src="{{ url('public/admin/js/jquery-ui-1.10.4.min.js') }}"></script>
    @yield('footer');
  </body>
</html>
