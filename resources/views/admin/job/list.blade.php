@extends('admin.layout.master')
@section('content')
  <!-- page start-->
<div class="row">
      <div class="col-lg-12">
        <div class="row">
        <div class="col-lg-3">
        <div class="panel-body">
          <a href="{!! route('admin.job.getAdd') !!}" class="btn btn-primary btn-lg btn-block">Add new Job</a>
        </div>
        </div>
        </div>
      </div>
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Jobs
              </header>
              <form class="navbar-form" action="" method="post">
                  <input class="form-control" placeholder="Search" type="text" name="search" id="search">
                  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                  <button class="btn btn-primary" type="submit">Search</button>
              </form>
              <table class="table table-striped table-advance table-hover">
               <tbody>
                  <tr>
                     <th><i class=""></i> #</th> 
                     <th><i class="icon_group"></i> Job Title</th>
                     <th><i class="icon_cogs"></i> Action</th>
                  </tr>
                  <?php $stt = 1; ?>
                  @foreach($data as $item)
                  <tr>
                    <td>{!! $stt++ !!}</td>
                     <td>{!! $item['job_title'] !!}</td>
                     <td>
                      <div class="btn-group">
                          <a class="btn btn-primary" href="#"><i class="icon_plus_alt2"></i></a>
                          <a class="btn btn-success" href="{!! URL::route('admin.job.getEdit', $item['id']) !!}"><i class="icon_check_alt2"></i> Edit</a>
                          <a class="btn btn-danger" href="{!! URL::route('admin.job.getDelete', $item['id']) !!}" onclick="return comfirmDelete('Are you sure?')"><i class="icon_close_alt2"></i> Delete</a>
                      </div>
                      </td>
                  </tr> 
                  @endforeach                           
               </tbody>
            </table>

            <div class="text-center">
                <ul class="pagination">
                    @if($data->currentPage() != 1)
                      <li><a href="{!! str_replace('/?', '?', $data->url($data->currentPage() - 1)) !!}">«</a></li>
                    @endif
                    @for($i = 1; $i <= $data->lastPage(); $i = $i + 1)
                      <li class="{!! ($data->currentPage() == $i) ? 'active' : '' !!}">
                        <a href="{!! str_replace('/?', '?', $data->url($i)) !!}">{!! $i !!}</a>
                      </li>
                    @endfor
                    @if($data->currentPage() != $data->lastPage())
                      <li><a href="{!! str_replace('/?', '?', $data->url($data->currentPage() + 1)) !!}">»</a></li>
                    @endif
                </ul>
            </div>
          </section>
      </div>
  </div>
  <!-- page end-->
          
@endsection
@section('footer')

@endsection