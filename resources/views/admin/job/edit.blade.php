@extends('admin.layout.master')
@section('content')
  <!-- page start-->
@include('admin.blocks.error')
<div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                 Edit Job
              </header>
              <div class="panel-body">
                  <div class="form">
                      <form class="form-validate form-horizontal " id="register_form" method="post" action="">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                          <div class="form-group ">
                              <label for="jobname" class="control-label col-lg-2">Job title <span class="required">*</span></label>
                              <div class="col-lg-10">
                                  <input class="form-control " id="jobname" name="jobname" type="text" value="{!! old('jobname', isset($data) ? $data['job_title'] : null) !!}"/>
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-lg-offset-2 col-lg-10">
                                  <button class="btn btn-primary" type="submit">Save</button>
                                  <a class="btn btn-default" href="{!! route('admin.job.getList') !!}">Cancel</a>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </section>
      </div>
  </div>
  <!-- page end-->

@endsection
          