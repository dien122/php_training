@extends('admin.layout.master')
@section('content')
  <!-- page start-->
@include('admin.blocks.error')
<div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                 Edit Department
              </header>
              <div class="panel-body">
                  <div class="form">
                      <form class="form-validate form-horizontal " id="register_form" method="post" action="">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                          <div class="form-group ">
                              <label for="departmentname" class="control-label col-lg-2">Department Name <span class="required">*</span></label>
                              <div class="col-lg-10">
                                  <input class="form-control " id="departmentname" name="departmentname" type="text" value="{!! old('departmentname', isset($data) ? $data['department_name'] : null) !!}"/>
                              </div>
                          </div>
                          <div class="form-group ">
                              <label for="officephone" class="control-label col-lg-2">Office Phone </label>
                              <div class="col-lg-10">
                                  <input class="form-control " id="officephone" name="officephone" type="text" value="{!! old('officephone', isset($data) ? $data['office_phone'] : null) !!}"/>
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-lg-offset-2 col-lg-10">
                                  <button class="btn btn-primary" type="submit">Save</button>
                                  <a class="btn btn-default" href="{!! route('admin.department.getList') !!}">Cancel</a>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </section>
      </div>
  </div>
  <!-- page end-->

@endsection
          