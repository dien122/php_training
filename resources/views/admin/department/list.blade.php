@extends('admin.layout.master')
@section('content')
  <!-- page start-->
<div class="row">
      <div class="col-lg-12">
        <div class="row">
        <div class="col-lg-3">
        <div class="panel-body">
          <a href="{!! route('admin.department.getAdd') !!}" class="btn btn-primary btn-lg btn-block">Add new Department</a>
        </div>
        </div>
        </div>
      </div>
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Departments
              </header>
              <form class="navbar-form" action="" method="post">
                  <input class="form-control" placeholder="Search" type="text" name="search" id="search">
                  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                  <button class="btn btn-primary" type="submit">Search</button>
              </form>
              <table class="table table-striped table-advance table-hover">
               <tbody>
                  <tr>
                     <th><i class=""></i> No.</th> 
                     <th><i class="icon_group"></i> Department Name</th>
                     <th><i class="icon_mobile"></i> Office Phone</th>
                     <th><i class="icon_cogs"></i> Action</th>
                  </tr>
                  <?php $stt = 1; ?>
                  @foreach($data as $item)
                  <tr>
                    <td>{!! $stt++ !!}</td>
                     <td>{!! $item['department_name'] !!}</td>
                     <td>{!! $item['office_phone'] !!}</td>
                     <td>
                      <div class="btn-group">
                          <a class="btn btn-primary" href="#"><i class="icon_plus_alt2"></i></a>
                          <a class="btn btn-success" href="{!! URL::route('admin.department.getEdit', $item['id']) !!}"><i class="icon_check_alt2"></i> Edit</a>
                          <a class="btn btn-danger" href="{!! URL::route('admin.department.getDelete', $item['id']) !!}" onclick="return comfirmDelete('Are you sure?')"><i class="icon_close_alt2"></i> Delete</a>
                      </div>
                      </td>
                  </tr> 
                  @endforeach                           
               </tbody>
            </table>

            <div class="text-center">
                <ul class="pagination">
                    @if($data->currentPage() != 1)
                      <li><a href="{!! str_replace('/?', '?', $data->url($data->currentPage() - 1)) !!}">«</a></li>
                    @endif
                    @for($i = 1; $i <= $data->lastPage(); $i = $i + 1)
                      <li class="{!! ($data->currentPage() == $i) ? 'active' : '' !!}">
                        <a href="{!! str_replace('/?', '?', $data->url($i)) !!}">{!! $i !!}</a>
                      </li>
                    @endfor
                    @if($data->currentPage() != $data->lastPage())
                      <li><a href="{!! str_replace('/?', '?', $data->url($data->currentPage() + 1)) !!}">»</a></li>
                    @endif
                </ul>
            </div>
          </section>
      </div>
  </div>
  <!-- page end-->
          
@endsection