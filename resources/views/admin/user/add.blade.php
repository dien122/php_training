@extends('admin.layout.master')
@section('content')
  <!-- page start-->
@include('admin.blocks.error')
<div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                 Add New User
              </header>
              <div class="panel-body">
                  <div class="form">
                      <form class="form-validate form-horizontal " id="register_form" method="post" action="" enctype="multipart/form-data">
                      	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
                          <div class="form-group ">
                              <label for="name" class="control-label col-lg-2">Full Name <span class="required">*</span></label>
                              <div class="col-lg-10">
                                  <input class="form-control " id="name" name="name" type="text" value="{{ old('name') }}" />
                              </div>
                          </div>
                          <div class="form-group ">
                              <label for="email" class="control-label col-lg-2">Email <span class="required">*</span></label>
                              <div class="col-lg-10">
                                  <input class="form-control " id="email" name="email" type="text" value="{{ old('email') }}" />
                              </div>
                          </div>
                          <div class="form-group ">
                              <label for="password" class="control-label col-lg-2">Password <span class="required">*</span></label>
                              <div class="col-lg-10">
                                  <input class="form-control " id="password" name="password" type="password" />
                              </div>
                          </div>
                          <div class="form-group ">
                              <label for="confirm_password" class="control-label col-lg-2">Confirm Password <span class="required">*</span></label>
                              <div class="col-lg-10">
                                  <input class="form-control " id="confirm_password" name="confirm_password" type="password" />
                              </div>
                          </div>
                          <div class="form-group ">
                              <label for="level" class="control-label col-lg-2">Level <span class="required">*</span></label>
                              <div class="col-lg-10">
                                  <div class="radios">
                                    @if(isSuperAdmin(Auth::user()->id, Auth::user()->level))
                                      <label class="label_radio" for="admin">
                                          <input name="author" id="admin" value="1" type="radio" /> Admin
                                      </label>
                                    @endif
                                      <label class="label_radio" for="member">
                                          <input name="author" id="member" value="2" type="radio" checked/> Member
                                      </label>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group ">
                              <label for="birth" class="control-label col-lg-2">Birth day </label>
                              <div class="col-lg-3">
                                  <input class="form-control " id="birth" name="birth" type="date" value="{!! old('birth') !!}"/>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="photo" class="control-label col-lg-2">Photo</label>
                              <div class="col-lg-3">
                                <input type="image" src="{{ url('public/admin/img/image_up.png') }}"/>
                                <input type="file" id="photo" name="photo" style="display: none;">
                              </div>
                          </div>
                          <div class="form-group ">
                              <label for="phone" class="control-label col-lg-2">Phone </label>
                              <div class="col-lg-10">
                                  <input class="form-control " id="phone" name="phone" type="text" value="{!! old('phone') !!}"/>
                              </div>
                          </div>
                          <div class="form-group ">
                              <label for="level" class="control-label col-lg-2">Sex <span class="required">*</span></label>
                              <div class="col-lg-10">
                                  <div class="radios">
                                      <label class="label_radio" for="female">
                                          <input name="sex" id="female" value="0" type="radio" checked /> Nữ
                                      </label>
                                      <label class="label_radio" for="male">
                                          <input name="sex" id="male" value="1" type="radio" /> Nam
                                      </label>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group">
                            <label for="username" class="control-label col-lg-2">Job</label>
                            <div class="col-lg-10">
                              <select class="form-control m-bot15" name="job">
                                  <option value="">Please Choose Job title</option>
                              @foreach($job as $item)
                                  @if(old('job') == $item['id'])
                                  <option value="{!! $item['id'] !!}" selected="selected">{!! $item['job_title'] !!}</option>
                                  @else
                                    <option value="{!! $item['id'] !!}">{!! $item['job_title'] !!}</option>
                                  @endif
                              @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="username" class="control-label col-lg-2">Department Name</label>
                            <div class="col-lg-10">
                              <select class="form-control m-bot15" name="depart">
                                  <option value="">Please Choose Department name</option>
                              @foreach($depart as $item)
                                  @if(old('depart') == $item['id'])
                                  <option value="{!! $item['id'] !!}" selected="selected">{!! $item['department_name'] !!}</option>
                                  @else
                                    <option value="{!! $item['id'] !!}">{!! $item['department_name'] !!}</option>
                                  @endif
                              @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-sm-2">Description</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control ckeditor" name="description" rows="6">{!! old('description') !!}</textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <div class="col-lg-offset-2 col-lg-10">
                                  <button class="btn btn-primary" type="submit">Save</button>
                                  <a class="btn btn-default" href="{!! route('admin.user.getList') !!}">Cancel</a>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </section>
      </div>
  </div>
  <!-- page end-->

@endsection()
          