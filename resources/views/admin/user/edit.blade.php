@extends('admin.layout.master')
@section('content')
  <!-- page start-->
@include('admin.blocks.error')
</div>
<div class="row">
  <!-- profile-widget -->
  <div class="col-lg-12">
      <div class="profile-widget profile-widget-info">
            <div class="panel-body">
              <div class="col-lg-2 col-sm-2">
                <h4>{!! $data['name'] !!}</h4>               
                <div class="follow-ava">
                    <img src="{!! url('public/admin/img/'.$data['photo']) !!}" alt="">
                </div>
                <h6>
                  @if(!empty($data['level']))
                      @if($data['id'] == 2 && $data['level'] == 1)
                        SupperAdmin
                      @elseif($data['level'] == 1)
                        Admin
                      @else
                        Member
                      @endif
                  @endif
                </h6>
              </div>
              <div class="col-lg-4 col-sm-4 follow-info">
                  <p>{!! $data['description'] !!}</p>
                  <p>
                      {!! $data['email'] !!}
                  </p>
                  <h6>
                      <span><i class="icon_clock_alt"></i>
                        <?php date_default_timezone_set('Asia/Ho_Chi_Minh'); 
                              echo date('H:i'); 
                        ?>
                      </span>
                      <span><i class="icon_calendar"></i>
                      <?php
                        echo date('d/m/Y');
                      ?>
                      </span>
                  </h6>
              </div>
            </div>
      </div>
  </div>
</div>
<!-- page start-->
<div class="row">
   <div class="col-lg-12">
      <section class="panel">
            <header class="panel-heading tab-bg-info">
                <ul class="nav nav-tabs">
                    <li class="">
                        <a data-toggle="tab" href="#profile">
                            <i class="icon-user"></i>
                            Profile
                        </a>
                    </li>
                    <li class="active">
                        <a data-toggle="tab" href="#edit-profile">
                            <i class="icon-envelope"></i>
                            Edit Profile
                        </a>
                    </li>
                </ul>
            </header>
            <div class="panel-body">
                <div class="tab-content">
                    <!-- profile -->
                    <div id="profile" class="tab-pane">
                      <section class="panel">
                        <div class="bio-graph-heading">
                                  {!! $data['description'] !!}
                        </div>
                        <div class="panel-body bio-graph-info">
                            <h1>Profile Info</h1>
                            <div class="row">
                                <div class="bio-row">
                                    <p><span>Full Name </span>: {!! $data['name'] !!} </p>
                                </div>                                            
                                <div class="bio-row">
                                    <p><span>Birthday</span>: {!! $data['birth'] !!}</p>
                                </div>
                                <div class="bio-row">
                                    <p><span>Email </span>: {!! $data['email'] !!}</p>
                                </div>
                                <div class="bio-row">
                                    <p><span>Sex </span>: {!! $data['sex'] ? 'Nam' : 'Nữ' !!}</p>
                                </div>
                                <div class="bio-row">
                                    <p><span>Department </span>: <?php $depart = DB::table('departments')->where('id', $data['department_id'])->first(); ?>
                                      @if(!empty($depart->department_name))
                                        {!! $depart->department_name !!}
                                      @endif
                                    </p>
                                </div>
                                <div class="bio-row">
                                    <p><span>Job </span>: <?php $job = DB::table('jobs')->where('id', $data['job_id'])->first(); ?>
                                      @if(!empty($job->job_title))
                                        {!! $job->job_title !!}
                                      @endif
                                    </p>
                                </div>
                                <div class="bio-row">
                                    <p><span>Phone </span>: {!! $data['phone'] !!}</p>
                                </div>
                            </div>
                        </div>
                      </section>
                        <section>
                            <div class="row">                                              
                            </div>
                        </section>
                    </div>
                    <!-- edit-profile -->
                    <div id="edit-profile" class="tab-pane active">
                      <section class="panel">                                          
                            <div class="panel-body bio-graph-info">
                                <h1> Edit Profile</h1>
                                <form class="form-validate form-horizontal " id="register_form" method="post" action="" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                    <div class="form-group ">
                                        <label for="name" class="control-label col-lg-2">Full Name <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="name" name="name" type="text" value="{{ old('name', $data['name']) }}" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="email" class="control-label col-lg-2">Email <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="email" name="email" type="text" value="{{ old('email', $data['email']) }}" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="level" class="control-label col-lg-2">Level <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <div class="radios">
                                              @if(isAdmin(Auth::user()->level))
                                                <label class="label_radio" for="admin">
                                                    <input name="author" id="admin" value="1" type="radio" 
                                                    @if($data['level'] == 1)
                                                      checked = "checked"
                                                    @endif
                                                     /> Admin
                                                </label>
                                              @endif
                                                <label class="label_radio" for="member">
                                                    <input name="author" id="member" value="2" type="radio" 
                                                    @if($data['level'] == 2)
                                                      checked = "checked"
                                                    @endif
                                                    /> Member
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="birth" class="control-label col-lg-2">Birth day </label>
                                        <div class="col-lg-3">
                                            <input class="form-control " id="birth" name="birth" type="date" value="{!! old('birth', $data['birth']) !!}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="photo" class="control-label col-lg-2">Photo</label>
                                        <div class="col-lg-3">
                                          <input type="file" id="exampleInputFile" name="photo">
                                          <p class="help-block">Example block-level help text here.</p>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="phone" class="control-label col-lg-2">Phone </label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="phone" name="phone" type="text" value="{!! old('phone', $data['phone']) !!}"/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="level" class="control-label col-lg-2">Sex <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <div class="radios">
                                                <label class="label_radio" for="female">
                                                    <input name="sex" id="female" value="0" type="radio" checked /> Nữ
                                                </label>
                                                <label class="label_radio" for="male">
                                                    <input name="sex" id="male" value="1" type="radio" /> Nam
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="job" class="control-label col-lg-2">Job</label>
                                      <div class="col-lg-10">
                                        <select class="form-control m-bot15" name="job">
                                            <option value="">Please Choose Job title</option>
                                            <?php $job = DB::table('jobs')->get(); ?>
                                        @foreach($job as $item)
                                            @if($data['job_id'] == $item->id))
                                            <option value="{!! $item->id !!}" selected="selected">{!! $item->job_title !!}</option>
                                            @else
                                              <option value="{!! $item->id !!}">{!! $item->job_title !!}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="depart" class="control-label col-lg-2">Department Name</label>
                                      <div class="col-lg-10">
                                        <select class="form-control m-bot15" name="depart">
                                            <option value="">Please Choose Department name</option>
                                            <?php $depart = DB::table('departments')->get(); ?>
                                        @foreach($depart as $item)
                                            @if($data['department_id'] == $item->id)
                                            <option value="{!! $item->id !!}" selected="selected">{!! $item->department_name !!}</option>
                                            @else
                                              <option value="{!! $item->id !!}">{!! $item->department_name !!}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Description</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control ckeditor" name="description" rows="6">{!! old('description', $data['description']) !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                            <a class="btn btn-default" href="{!! route('admin.user.getList') !!}">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
   </div>
</div>
  <!-- page end-->
@endsection