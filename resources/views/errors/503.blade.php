@extends('admin.layout.auth')
@section('content')
<body>
    <div class="page-404">
        <p class="text-404">404</p>
        <h2>Aww Snap!</h2>
        <p>Something went wrong or that page doesn’t exist yet. <br><a href="{{ URL::route('admin.department.getList') }}">Return Home</a></p>
    </div>
</body>
@endsection
