@extends('admin.layout.auth')
@section('content')
<!-- login start-->
<body class="login-img3-body">
    <div class="container">
        <form class="login-form" action="{{ URL::route('email') }}" method="post"> 
            {!! csrf_field() !!}  
            @include('admin.blocks.error')     
            <div class="login-wrap">
                <p class="login-img"><i class="icon_lock_alt"></i></p>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                    <input type="email" name="email" class="form-control" placeholder="Email" autofocus value="{{ old('email') }}">
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Send Password Reset Link</button>
            </div>
        </form>
    </div>
</body>
<!-- login end-->
@endsection