@extends('admin.layout.auth')
@section('content')
<!-- login start-->
<body class="login-img3-body">
    <div class="container">
        <form class="login-form" action="{{ URL::route('password') }}" method="post"> 
            {!! csrf_field() !!}  
            <input type="hidden" name="token" value="{{ $token }}">
            @include('admin.blocks.error')     
            <div class="login-wrap">
                <p class="login-img"><i class="icon_lock_alt"></i></p>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                    <input type="email" name="email" class="form-control" placeholder="Email" autofocus value="{{ old('email') }}">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                    <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                    <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation">
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Reset Password</button>
            </div>
        </form>
    </div>
</body>
<!-- login end-->
@endsection