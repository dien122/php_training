@extends('admin.layout.auth')
@section('content')
<!-- login start-->
<body class="login-img3-body">
    <div class="container">
        <form class="login-form" action="" method="post"> 
            {!! csrf_field() !!}  
            @include('admin.blocks.error')     
            <div class="login-wrap">
                <p class="login-img"><i class="icon_lock_alt"></i></p>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                    <input type="email" name="email" class="form-control" placeholder="Email" autofocus value="{{ old('email') }}">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                    <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                </div>
                <label class="checkbox">
                    <input type="checkbox" value="remember" name="remember"> Remember me
                    <span class="pull-right"> <a href="{{ URL::route('email') }}"> Forgot Password?</a></span>
                </label>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
            </div>
        </form>
    </div>
</body>
<!-- login end-->
@endsection