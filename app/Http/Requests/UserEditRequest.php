<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserEditRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pattern = '#^[a-zA-Z0-9\-_\.\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$#';
        $id = $this->id;
        return [
            'name' => 'required|min:4|max:20|regex:'.$pattern,
            'email' => 'required|unique:users,email,'.$id.'|email',
            'birth' => 'required|date',
            'photo' => 'mimes:jpeg,bmp,png,gif,jpg',
            'phone' => 'numeric',
            'job'  => 'required',
            'depart'  => 'required',
            'description'  => 'min:10|max:400',
        ];
    }

    /**
     * Get the message error that apply to the request.
     *
     * @return array
     */
    public function messages () 
    {
        return [
            'name.required' => 'Please Enter Name',
            'name.unique' => 'This Name is Exist',
            'name.regex'  => 'The jobname format is invalid. You can not use special characters, spaces. You can use letters, number, underscore and dash',
            'email.required' => 'Please Enter email',
            'email.unique' => 'This Email is Exist',
            'name.required' => 'Please Enter Password',
            'email.email' => 'this is not email',
            'confirm_password.required' => 'Please Enter Confirm Password',
            'confirm_password.same' => 'Two password do not match',
        ];
    }
}
