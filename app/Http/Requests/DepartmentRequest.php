<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Department;

class DepartmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pattern = '#^[a-zA-Z0-9\-_\.\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$#';
        $id = $this->id;
        return [
            'departmentname' => 'required|regex:'.$pattern.'|min:4|max:40|unique:departments,department_name,'.$id,
            'officephone' => 'numeric',
        ];
    }

    /**
     * Get the message error that apply to the request.
     *
     * @return array
     */
    public function messages ()
    {
        return [
            'departmentname.required' => 'Please Enter Department Name',
            'departmentname.unique' => 'This Name Department is Exist',
            'departmentname.regex' => 'The departmentname format is invalid. You can not use special characters'
        ];
    }
}
