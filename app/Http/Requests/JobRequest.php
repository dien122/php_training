<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JobRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $pattern = '#^[a-zA-Z0-9\-_\.\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$#';
        $id = $this->id;
        return [
            'jobname' => 'required|regex:'.$pattern.'|min:4|max:40|unique:jobs,job_title,'.$id
        ];
    }

    public function messages ()
    {
        return [
            'jobname.required' => 'Please Enter Job Title',
            'jobname.unique' => 'This Name Job is Exist',
            'jobname.regex' => 'The jobname format is invalid. You can not use special characters'
        ];
    }
}
