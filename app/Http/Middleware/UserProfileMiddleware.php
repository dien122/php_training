<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserProfileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle(Request $request, Closure $next)
    {
        if(!Auth::check() or ($request->user()->level != 1 && $request->user()->id != Auth::user()->id)) {
            return redirect('unauthorized');
        }
        return $next($request);
    }
}