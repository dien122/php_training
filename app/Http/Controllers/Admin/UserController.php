<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserEditRequest;
use App\Models\User;
use App\Models\Job;
use App\Models\Department;
use Auth;
use Session;
use DB;

class UserController extends Controller
{
    /**
     * The User instance.
     *
     * @var App\Models\User
     */
    protected $user;

    /**
     * Create a new UserController instance.
     *
     * @param  App\Models\User $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Create a new user instance after a valid login.
     *
     * @return view()
     */
    public function getAdd ()
    {
        $job = Job::select('id', 'job_title')->orderBy('job_title', 'ASC')->get()->toArray();
        $depart = Department::select('id', 'department_name')->orderBy('department_name', 'ASC')
                                                             ->get()->toArray();
        return view('admin.user.add', compact('job', 'depart')); 
    }

    /**
     * Create a new user instance after get a validator for an incoming request.
     *
     * @param  UserRequest  $request
     * @return redirect()
     */
    public function postAdd (UserRequest $request) 
    {
        $this->user->store($request);
        return redirect()->route('admin.user.getList')
                         ->with(['flash-level'=>'success','flash-message'=>'Success !! Complete add User']);
    }

    /**
     * Display a list of user. Filter data for search
     *
     * @return view()
     */
    public function getList ()
    {
        $search = null;
        if(Session::has('search'))
        {
            $search = Session::get('search');
        } 
        $data = $this->user->listAll($search);       
        return view('admin.user.list', compact('data'));
    }

    /**
     * Data processing for search
     *
     * @param  Request  $request
     * @return redirect()
     */
    public function postList (Request $request)
    {
        $search = $request->search;
        return redirect()->route('admin.user.getList')->with(['search'=>$search]);
    }

    /**
     * Delete data of users
     *
     * @param  var $id
     * @return redirect()
     */
    public function getDelete ($id)
    {
        $redirect = $this->department->deleteSubmit($id);
        return $redirect;
    }

    /**
     * Edit data of users
     *
     * @param  var $id
     * @return view()
     */
    public function getEdit ($id)
    {
        $data = User::findOrFail($id)->toArray();
        if (!isSuperAdmin(Auth::user()->id, Auth::user()->level) && (isSuperAdmin($id, $data['level']) || (isAdmin($data['level']) && Auth::user()->id != $id))) 
        {
            return redirect()->route('admin.user.getList')
                             ->with(['flash-level'=>'danger','flash-message'=>'Sorry !! You can not access']);
        }
        $job = Job::select('id', 'job_title')->orderBy('job_title', 'ASC')->get()->toArray();
        $depart = Department::select('id', 'department_name')->orderBy('department_name', 'ASC')
                                                             ->get()->toArray();
        return view('admin.user.edit', compact('data', 'id', 'depart', 'job'));  
    }

    /**
     * Edit the user instance after get a validator for an incoming request.
     *
     * @param  UserRequest  $request
     * @return redirect()
     */
    public function postEdit (UserEditRequest $request, $id)
    {
        $this->user->edit($request, $id);
        if($id == Auth::user()->id)
        {
            return redirect('admin/user/edit/'.$id)->with(['flash-level'=>'success','flash-message'=>'Success !! Complete edit User']);
        }
        return redirect()->route('admin.user.getList')
                         ->with(['flash-level'=>'success','flash-message'=>'Success !! Complete edit User']);
    }
}
