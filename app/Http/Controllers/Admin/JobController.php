<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\JobRequest;
use App\Models\Job;
use Session;
use Auth;
use DB;

class JobController extends Controller
{
    /**
     * The Job instance.
     *
     * @var App\Models\Job
     */
    protected $job;

    /**
     * Create a new JobController instance.
     *
     * @param  App\Models\Job $job
     * @return void
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }
    /**
     * Create a new job instance after a valid login.
     *
     * @return view()
     */
    public function getAdd ()
    {
    	return view('admin.job.edit');
    }

    /**
     * Create a new job instance after get a validator for an incoming request.
     *
     * @param  JobRequest  $request
     * @return redirect()
     */
    public function postAdd (JobRequest $request) 
    {
		$this->job->store($request);
		return redirect()->route('admin.job.getList')
                         ->with(['flash-level'=>'success','flash-message'=>'Success !! Complete add Job']);
    }

    /**
     * Display a list of jobs. Filter data for search
     *
     * @return view()
     */
    public function getList ()
    {   
        $search = null;
        if(Session::has('search'))
        {
            $search = Session::get('search');
        }
        $data = $this->job->listAll($search);        
    	return view('admin.job.list', compact('data'));
    }

    /**
     * Data processing for search
     *
     * @param  Request  $request
     * @return redirect()
     */
    public function postList (Request $request)
    {
        $search = $request->search;
        return redirect()->route('admin.job.getList')->with(['search'=>$search]);
    }

    /**
     * Delete data of jobs
     *
     * @param  var $id
     * @return redirect()
     */
    public function getDelete ($id)
    {
        $redirect = $this->job->deleteSubmit($id);
    	return $redirect;
    }

    /**
     * Edit data of jobs
     *
     * @param  var $id
     * @return view()
     */
    public function getEdit ($id)
    {
    	$data = Job::findOrFail($id)->toArray();
    	return view('admin.job.edit', compact('data'));
    }

    /**
     * Edit the job instance after get a validator for an incoming request.
     *
     * @param  JobRequest  $request
     * @return redirect()
     */
    public function postEdit (JobRequest $request, $id)
    {
        $this->job->edit($request, $id);
        DB::commit();
		return redirect()->route('admin.job.getList')
                         ->with(['flash-level'=>'success','flash-message'=>'Success !! Complete edit Job']);
    }
}
