<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\DepartmentRequest;
use App\Models\Department;
use Auth;
use Session;
use DB;

class DepartmentController extends Controller
{
    /**
     * The Department instance.
     *
     * @var App\Models\Department
     */
    protected $department;

    /**
     * Create a new departmentController instance.
     *
     * @param  App\Models\Department $department
     * @return void
     */
    public function __construct(Department $department)
    {
        $this->department = $department;
    }
    /**
     * Create a new department instance after a valid login.
     *
     * @return view()
     */
    public function getAdd () 
    {
        return view('admin.department.edit');
    }

    /**
     * Create a new department instance after get a validator for an incoming request.
     *
     * @param  DepartmentRequest  $request
     * @return redirect()
     */
    public function postAdd (DepartmentRequest $request) 
    {
        $this->department->store($request);
        return redirect()->route('admin.department.getList')
                         ->with(['flash-level'=>'success','flash-message'=>'Success !! Complete add Department']);
    }

    /**
     * Display a list of departments. Filter data for search
     *
     * @return view()
     */
    public function getList ()
    {   
        $search = null;
        if(Session::has('search'))
        {
            $search = Session::get('search');
        }
        $data = $this->department->listAll($search);
        return view('admin.department.list', compact('data'));
    }

    /**
     * Data processing for search
     *
     * @param  Request  $request
     * @return redirect()
     */
    public function postList (Request $request)
    {
        $search = $request->search;
        return redirect()->route('admin.department.getList')
                         ->with(['search'=>$search]);
    }

    /**
     * Delete data of departments
     *
     * @param  var $id
     * @return redirect()
     */
    public function getDelete ($id)
    {
        $redirect = $this->department->deleteSubmit($id);
        return $redirect;
    }

    /**
     * Edit data of departments
     *
     * @param  var $id
     * @return view()
     */
    public function getEdit ($id)
    {
        $data = Department::findOrFail($id)->toArray();
        return view('admin.department.edit', compact('data'));
    }

    /**
     * Edit the department instance after get a validator for an incoming request.
     *
     * @param  DepartmentRequest  $request
     * @return redirect()
     */
    public function postEdit (DepartmentRequest $request, $id)
    {
        $this->department->edit($request, $id);
        return redirect()->route('admin.department.getList')
                         ->with(['flash-level'=>'success','flash-message'=>'Success !! Complete edit Department']);
    }
}
