<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Admin Routes Group
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin', 'middleware'=>'auth'], function () {
	/**
	* Department Routes Group
	*/
	Route::group(['prefix'=>'department'], function () {
		Route::get('list', ['as'=>'admin.department.getList', 'uses'=>'Admin\DepartmentController@getList']);
		Route::post('list', ['as'=>'admin.department.postList', 'uses'=>'Admin\DepartmentController@postList']);
		Route::get('add', ['middleware'=>'user.permission', 'as'=>'admin.department.getAdd', 'uses'=>'Admin\DepartmentController@getAdd']);
		Route::post('add', ['as'=>'admin.department.postAdd', 'uses'=>'Admin\DepartmentController@postAdd']);
		Route::get('delete/{id}', ['middleware'=>'user.permission', 'as'=>'admin.department.getDelete', 'uses'=>'Admin\DepartmentController@getDelete']);
		Route::get('edit/{id}', ['middleware'=>'user.permission', 'as'=>'admin.department.getEdit', 'uses'=>'Admin\DepartmentController@getEdit']);
		Route::post('edit/{id}', ['as'=>'admin.department.postEdit', 'uses'=>'Admin\DepartmentController@postEdit']);
	});

	/**
	* Job Routes Group
	*/
	Route::group(['prefix'=>'job'], function () {
		Route::get('list', ['as'=>'admin.job.getList', 'uses'=>'Admin\JobController@getList']);
		Route::get('add', ['middleware'=>'user.permission', 'as'=>'admin.job.getAdd', 'uses'=>'Admin\JobController@getAdd']);
		Route::post('add', ['as'=>'admin.job.postAdd', 'uses'=>'Admin\JobController@postAdd']);
		Route::get('delete/{id}', ['middleware'=>'user.permission', 'as'=>'admin.job.getDelete', 'uses'=>'Admin\JobController@getDelete']);
		Route::get('edit/{id}', ['middleware'=>'user.permission', 'as'=>'admin.job.getEdit', 'uses'=>'Admin\JobController@getEdit']);
		Route::post('edit/{id}', ['as'=>'admin.job.postEdit', 'uses'=>'Admin\JobController@postEdit']);
		Route::post('list', ['as'=>'admin.job.postList', 'uses'=>'Admin\JobController@postList']);
	});

	/**
	* User Routes Group
	*/
	Route::group(['prefix'=>'user'], function () {
		Route::get('list', ['middleware'=>'user.permission', 'as'=>'admin.user.getList', 'uses'=>'Admin\UserController@getList']);
		Route::post('list', ['middleware'=>'user.permission', 'as'=>'admin.user.postList', 'uses'=>'Admin\UserController@postList']);
		Route::get('add', ['middleware'=>'user.permission', 'as'=>'admin.user.getAdd', 'uses'=>'Admin\UserController@getAdd']);
		Route::post('add', ['middleware'=>'user.permission', 'as'=>'admin.user.postAdd', 'uses'=>'Admin\UserController@postAdd']);
		Route::get('delete/{id}', ['middleware'=>'user.permission', 'as'=>'admin.user.getDelete', 'uses'=>'Admin\UserController@getDelete']);
		Route::get('edit/{id}', ['middleware'=>'user.profile.permission', 'as'=>'admin.user.getEdit', 'uses'=>'Admin\UserController@getEdit']);
		Route::post('edit/{id}', ['middleware'=>'user.profile.permission', 'as'=>'admin.user.postEdit', 'uses'=>'Admin\UserController@postEdit']);
	});


});

/*
|--------------------------------------------------------------------------
| Authentication Routes Group
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'auth'], function () {
	// Authentication routes...
	Route::get('login', ['as'=> 'auth.login', 'uses'=>'Auth\AuthController@getLogin']);
	Route::post('login', ['as'=> 'auth.login', 'uses'=>'Auth\AuthController@postLogin']);
	Route::get('logout', ['as'=> 'auth.logout', 'uses'=>'Auth\AuthController@getLogout']);

	// Password reset link request routes...
	Route::get('password/email', ['as'=> 'email', 'uses'=>'Auth\PasswordController@getEmail']);
	Route::post('password/email', ['as'=> 'email', 'uses'=>'Auth\PasswordController@postEmail']);

	// Password reset routes...
	Route::get('password/reset/{token}', ['as'=> 'password', 'uses'=>'Auth\PasswordController@getReset']);
	Route::post('password/reset', ['as'=> 'password', 'uses'=>'Auth\PasswordController@postReset']);
});

/**
 * Route tasks redirect users without permission access
 */
Route::get('unauthorized',function(){
  return redirect()->route('admin.department.getList')->with(['flash-level'=>'danger','flash-message'=>'Sorry!! You can not access']);
});

/**
 * Route tasks redirect users without permission access
 */
Route::get('error',function(){
  return view('errors.503');
});
