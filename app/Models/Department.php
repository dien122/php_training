<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Department extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'departments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['department_name', 'office_phone'];

    /**
     * Create table relationships with users
     *
     * @var array
     */
    public function user ()
    {
    	return $this->hasMany('App\Models\User');
    }

    /**
	 * Store a department.
	 *
	 * @param  array $inputs
	 * @return void
	 */
	public function store($inputs)
	{
		$this->department_name = $inputs['departmentname'];
		$this->office_phone = $inputs['officephone'];
		$this->save();
	}

	/**
	 * Update a department.
	 *
	 * @param  array $inputs
	 * @return void
	 */
	public function edit($inputs, $id)
	{
		$department = $this->where('id', $id)->firstOrFail();
		$department->department_name = $inputs['departmentname'];
		$department->office_phone = $inputs['officephone'];
		$department->save();
	}

	/**
	 * Delete a department.
	 *
	 * @param  array $inputs
	 * @return void
	 */
	public function deleteSubmit($id)
	{
		$department = $this->where('id', $id)->firstOrFail();
		/**
         * Can not delete the data related to supper admin
         */
		$user = Department::find($id)->user->toArray();
		foreach ($user as $value) {
			if(isSuperAdmin($value['id'], $value['level'])) {
            return redirect()->route('admin.department.getList')
            				 ->with(['flash-level'=>'danger','flash-message'=>'Sorry !! You can not delete this department. Because This department is connecting with SupperAdmin.']);
			}
		}
		$department->delete($id);
		return redirect()->route('admin.department.getList')
		                 ->with(['flash-level'=>'success','flash-message'=>'Success !! Complete Delete Department']);
	}

	/**
	 * Display a list of departments.
	 *
	 * @param  array $inputs
	 * @return void
	 */
	public function listAll($search = null)
	{
		$data = $this->orderBy('department_name', 'ASC')->paginate(PER_PAGE);
		if(!empty($search))
        {
            $data = $this->where('department_name', 'LIKE', '%' . $search . '%')
                         ->orderBy('department_name', 'ASC')
                         ->paginate(PER_PAGE);
        }
        return $data;
	}
}
