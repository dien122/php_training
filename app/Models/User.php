<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Hash;
use File;
use Auth;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Create table relationships with department
     *
     * @var array
     */
    public function department ()
    {
        return $this->belongto('App\Models\Department');
    }

    /**
     * Create table relationships with jobs
     *
     * @var array
     */
    public function job ()
    {
        return $this->belongto('App\Models\Job');
    }

    /**
     * Store a job.
     *
     * @param  array $inputs
     * @return void
     */
    public function store($inputs)
    {
        $this->name = $inputs['name'];
        $this->email = $inputs['email'];
        $this->password = Hash::make($inputs['password']);
        $this->level = $inputs['author'];
        $this->birth = $inputs['birth'];
        $this->phone = $inputs['phone'];
        $this->sex = $inputs['sex'];
        $this->job_id = $inputs['job'];
        $this->department_id = $inputs['depart'];
        $this->description = $inputs['description'];
        $this->remember_token = $inputs['_token'];
        if (!empty($inputs->file('photo'))) {
            $file_image = $inputs->file('photo')->getClientOriginalName();
            $this->photo = $file_image;
            $inputs->file('photo')->move('public/admin/img/',$file_image);
        }
        $this->save();
    }

    /**
     * Update a job.
     *
     * @param  array $inputs
     * @return void
     */
    public function edit($inputs, $id)
    {
        $user = $this->where('id', $id)->firstOrFail();
        $user->name = $inputs['name'];
        $user->email = $inputs['email'];
        $user->password = Hash::make($inputs['password']);
        $user->level = $inputs['author'];
        $user->birth = $inputs['birth'];
        $user->phone = $inputs['phone'];
        $user->sex = $inputs['sex'];
        $user->job_id = $inputs['job'];
        $user->department_id = $inputs['depart'];
        $user->description = $inputs['description'];
        $user->remember_token = $inputs['_token'];
        if (!empty($inputs->file('photo'))) {
            File::delete('public/admin/img/'.$user->photo);
            $file_image = $inputs->file('photo')->getClientOriginalName();
            $user->photo = $file_image;
            $inputs->file('photo')->move('public/admin/img/',$file_image);
        }
        $user->save();
    }

    /**
     * Delete a user.
     *
     * @param  array $inputs
     * @return void
     */
    public function deleteSubmit($id)
    {
        $user = $this->where('id', $id)->firstOrFail();
        /**
         * Can not delete the data related to supper admin
         */
        if(isSuperAdmin($id, $user->level) || (isAdmin($user->level) && isAdmin(Auth::$user->level))) {
            return redirect()->route('admin.user.getList')->with(['flash-level'=>'danger','flash-message'=>'Sorry !! You can not delete this user. Because This user is connecting with SupperAdmin.']);
        }
        $user->delete($id);
        File::delete('public/admin/img/'.$user->photo);
        return redirect()->route('admin.user.getList')
                         ->with(['flash-level'=>'success','flash-message'=>'Success !! Complete Delete user']);
    }

    /**
     * Display a list of jobs.
     *
     * @param  array $inputs
     * @return void
     */
    public function listAll($search = null)
    {
        $data = $this->orderBy('name', 'ASC')->paginate(PER_PAGE);
        if(!empty($search))
        {
            $data = $this->where('name', 'LIKE', '%' . $search . '%')
                         ->orderBy('name', 'ASC')
                         ->paginate(PER_PAGE);
        }
        return $data;
    }
}
