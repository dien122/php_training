<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['job_title'];

    /**
     * Create table relationships with users
     *
     * @var array
     */
    public function user ()
    {
    	return $this->hasMany('App\Models\User');
    }

    /**
	 * Store a job.
	 *
	 * @param  array $inputs
	 * @return void
	 */
	public function store($inputs)
	{
		$this->job_title = $inputs['jobname'];
		$this->save();
	}

	/**
	 * Update a job.
	 *
	 * @param  array $inputs
	 * @return void
	 */
	public function edit($inputs, $id)
	{
		$job = $this->where('id', $id)->firstOrFail();
		$job->job_title = $inputs['jobname'];
		$job->save();
	}

	/**
	 * Delete a job.
	 *
	 * @param  array $inputs
	 * @return void
	 */
	public function deleteSubmit($id)
	{
		$job = $this->where('id', $id)->firstOrFail();
		/**
         * Can not delete the data related to supper admin
         */
		$user = Job::find($id)->user->toArray();
		foreach ($user as $value) {
			if(isSuperAdmin($value['id'], $value['level'])) {
            return redirect()->route('admin.job.getList')
            				 ->with(['flash-level'=>'danger','flash-message'=>'Sorry !! You can not delete this job. Because This job is connecting with SupperAdmin.']);
			}
		}
		$job->delete($id);
		return redirect()->route('admin.job.getList')->with(['flash-level'=>'success','flash-message'=>'Success !! Complete Delete job']);
	}

	/**
	 * Display a list of jobs.
	 *
	 * @param  array $inputs
	 * @return void
	 */
	public function listAll($search = null)
	{
		$data = $this->orderBy('job_title', 'ASC')->paginate(PER_PAGE);
		if(!empty($search))
        {
            $data = $this->where('job_title', 'LIKE', '%' . $search . '%')->orderBy('job_title', 'ASC')->paginate(PER_PAGE);
        }
        return $data;
	}
}
