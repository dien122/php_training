<?php
	/**
    * Find out if User is an SuperAdmin, based on if has any roles
    *
    * @return boolean
    */
    function isSuperAdmin($id, $level)
    {
        if (($id == 2) && ($level == 1)) {
            return true;
        } 
        return false;
    }

    /**
    * Find out if User is an Admin, based on if has any roles
    *
    * @return boolean
    */
    function isAdmin($level)
    {
        if ($level == 1) {
            return true;
        } 
        return false;
    }