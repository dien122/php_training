<?php namespace App\Repositories;

use App\Models\Department;

class DepartmentRepository extends BaseRepository {

	/**
	 * Create a new DepartmentRepository instance.
	 *
	 * @param  App\Models\Department $department
	 * @return void
	 */
	public function __construct(Department $department)
	{
		$this->model = $department;
	}

	/**
	 * Get departments collection.
	 *
	 * @param  int  $n
	 * @return Illuminate\Support\Collection
	 */
	public function index($n)
	{
		return $this->model
		->with('post', 'user')
		->oldest('seen')
		->latest()
		->paginate($n);
	}

	/**
	 * Store a department.
	 *
	 * @param  array $inputs
	 * @param  int   $user_id
	 * @return void
	 */
 	public function store($inputs, $user_id)
	{
		$department = new $this->model;	

		$department->content = $inputs['departments'];
		$department->post_id = $inputs['post_id'];
		$department->user_id = $user_id;

		$department->save();
	}

	/**
	 * Update a department.
	 *
	 * @param  string $departmentaire
	 * @param  int    $id
	 * @return void
	 */
 	public function updateContent($content, $id)
	{
		$department = $this->getById($id);	

		$department->content = $content;

		$department->save();
	}

	/**
	 * Update a department.
	 *
	 * @param  bool  $vu
	 * @param  int   $id
	 * @return void
	 */
	public function update($seen, $id)
	{
		$department = $this->getById($id);

		$department->seen = $seen == 'true';

		$department->save();
	}

}